package com.example.firebasedemo.injection.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.firebasedemo.injection.scope.ViewModelScope
import com.example.firebasedemo.ui.addNote.AddNoteViewModel
import com.example.firebasedemo.ui.home.HomeViewModel
import com.example.firebasedemo.ui.login.LoginViewModel
import com.example.firebasedemo.ui.signUp.SignUpViewModel
import com.example.firebasedemo.ui.splash.SplashViewModel
import com.example.firebasedemo.utils.ViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelFactoryModule {

  @Binds
  @IntoMap
  @ViewModelScope(HomeViewModel::class)
  abstract fun bindHomeViewModel(homeViewModel: HomeViewModel): ViewModel

  @Binds
  @IntoMap
  @ViewModelScope(AddNoteViewModel::class)
  abstract fun bindAddNoteViewModel(addNoteViewModel: AddNoteViewModel): ViewModel

  @Binds
  @IntoMap
  @ViewModelScope(SignUpViewModel::class)
  abstract fun bindSignUpViewModel(signUpViewModel: SignUpViewModel): ViewModel

  @Binds
  @IntoMap
  @ViewModelScope(LoginViewModel::class)
  abstract fun bindLoginFragmentViewModel(loginViewModel: LoginViewModel): ViewModel

  @Binds
  @IntoMap
  @ViewModelScope(SplashViewModel::class)
  abstract fun bindSplashFragmentViewModel(splashViewModel: SplashViewModel): ViewModel

  @Binds
  internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}
