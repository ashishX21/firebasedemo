package com.example.firebasedemo.injection.qualifiers

import javax.inject.Qualifier

@Retention
@Qualifier
annotation class ActivityContext