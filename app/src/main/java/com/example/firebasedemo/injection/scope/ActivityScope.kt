package com.example.firebasedemo.injection.scope

import javax.inject.Scope

@Scope
annotation class ActivityScope
