package com.example.firebasedemo.injection.module

import com.example.firebasedemo.data.source.FirebaseAuthSource
import com.example.firebasedemo.data.source.FirebaseDatabaseSource
import com.example.firebasedemo.data.source.FirebaseStorageSource
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class NetworkModule {

  @Provides
  @Singleton
  fun provideFirebaseAuthSource(): FirebaseAuthSource {
    return FirebaseAuthSource(FirebaseAuth.getInstance())
  }

  @Provides
  @Singleton
  fun provideFirebaseDatabaseSource(): FirebaseDatabaseSource {
    return FirebaseDatabaseSource(FirebaseFirestore.getInstance())
  }

  @Provides
  @Singleton
  fun provideFirebaseStorageSource(): FirebaseStorageSource {
    return FirebaseStorageSource(FirebaseStorage.getInstance())
  }

}
