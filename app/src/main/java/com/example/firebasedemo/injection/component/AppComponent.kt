package com.example.firebasedemo.injection.component

import android.content.Context
import com.example.firebasedemo.injection.module.ViewModelFactoryModule
import com.example.firebasedemo.BaseApplication
import com.example.firebasedemo.injection.module.ActivityBindingModule
import com.example.firebasedemo.injection.module.AppModule
import com.example.firebasedemo.injection.module.NetworkModule
import com.example.firebasedemo.injection.module.PreferencesModule
import com.example.firebasedemo.injection.qualifiers.ApplicationContext
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [AppModule::class, PreferencesModule::class, ViewModelFactoryModule::class,
      AndroidSupportInjectionModule::class, ActivityBindingModule::class, NetworkModule::class]
)
interface AppComponent : AndroidInjector<BaseApplication> {
  @Component.Builder
  abstract class Builder : AndroidInjector.Builder<BaseApplication>() {
    @BindsInstance
    abstract fun appContext(@ApplicationContext context: Context)

    override fun seedInstance(instance: BaseApplication?) {
      appContext(instance!!.applicationContext)
    }
  }
}