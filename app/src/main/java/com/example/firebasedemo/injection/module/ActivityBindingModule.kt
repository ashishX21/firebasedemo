package com.example.firebasedemo.injection.module

import com.example.firebasedemo.injection.scope.ActivityScope
import com.example.firebasedemo.ui.addNote.AddNoteActivity
import com.example.firebasedemo.ui.addNote.AddNoteActivityModule
import com.example.firebasedemo.ui.home.HomeActivity
import com.example.firebasedemo.ui.home.HomeActivityModule
import com.example.firebasedemo.ui.login.LoginActivity
import com.example.firebasedemo.ui.login.LoginActivityModule
import com.example.firebasedemo.ui.signUp.SignUpActivity
import com.example.firebasedemo.ui.signUp.SignUpActivityModule
import com.example.firebasedemo.ui.splash.SplashActivity
import com.example.firebasedemo.ui.splash.SplashActivityModule
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.android.support.DaggerAppCompatActivity

/**
 * To create dependencies for a specific activity, don't extend the required activity module with #ActivityModule,
 * instead create a plain module and include #BaseActivityModule in the annotation.
 */
@Module
abstract class ActivityBindingModule {

  @ActivityScope
  @ContributesAndroidInjector(modules = [AddNoteActivityModule::class])
  internal abstract fun bindAddNoteActivity(): AddNoteActivity

  @ActivityScope
  @ContributesAndroidInjector(modules = [HomeActivityModule::class])
  internal abstract fun bindHomeActivity(): HomeActivity

  @ActivityScope
  @ContributesAndroidInjector(modules = [LoginActivityModule::class])
  internal abstract fun bindLoginActivity(): LoginActivity

  @ActivityScope
  @ContributesAndroidInjector(modules = [SignUpActivityModule::class])
  internal abstract fun bindSignUpActivity(): SignUpActivity

  @ActivityScope
  @ContributesAndroidInjector(modules = [SplashActivityModule::class])
  internal abstract fun bindSplashActivity(): SplashActivity

}

@Module(includes = [BaseActivityModule::class])
abstract class ActivityModule<in T : DaggerAppCompatActivity> {
  @Binds
  @ActivityScope
  internal abstract fun bindActivity(activity: T): DaggerAppCompatActivity
}

/**
 * Activity specific common dependencies should be placed here
 */
@Module
open class BaseActivityModule {
//  @ActivityScope
//  @Provides
//  internal fun provideRxPermissions(activity: DaggerAppCompatActivity) = RxPermissions(
//      activity
//  )
}