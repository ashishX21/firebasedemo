package com.example.firebasedemo

import java.lang.Exception

object AppConstants{
  const val USERS = "users"
  const val NOTES ="notes"

  enum class NetworkState {
    INIT,
    LOADING,
    SUCCESS,
    FAILED,
    NO_INTERNET
  }

  sealed class NetworkResult<out T> {
    data class Success<T>(val body: T) : NetworkResult<T>()
    data class Failure<T>(val errorResponse: Exception? = null) : NetworkResult<T>()
  }
}