package com.example.firebasedemo.data.source

import android.net.Uri
import com.example.firebasedemo.data.model.Note
import com.example.firebasedemo.data.model.NoteImage
import com.google.firebase.storage.FirebaseStorage
import io.reactivex.Completable
import io.reactivex.Single

class FirebaseStorageSource(private val storage: FirebaseStorage) {

  private val storageRef by lazy { storage.reference }

  fun uploadFile(
    path: String,
    noteImage: NoteImage
  ) = Single.create<String> { emitter ->
    val imageRef =
      storageRef.child("$path/${noteImage.fileName}")
    imageRef.putFile(Uri.parse(noteImage.uriString))
        .continueWithTask { task ->
          if (!task.isSuccessful) {
            task.exception?.let {
              emitter.onError(it)
            }
          }
          imageRef.downloadUrl
        }
        .addOnCompleteListener { task ->
          if (!task.isSuccessful) {
            task.exception?.let {
              emitter.onError(it)
            }
          } else {
            task.result?.let {
              emitter.onSuccess(it.toString())
            }
          }
        }
  }

  fun deleteFile(
    path: String,
    noteImage: NoteImage
  ) = Completable.create { emitter ->
    val imageRef =
      storageRef.child("$path/${noteImage.fileName}")
    imageRef.delete()
        .addOnCompleteListener { task ->
          if (!task.isSuccessful) {
            task.exception?.let {
              emitter.onError(it)
            }
          } else {
            emitter.onComplete()
          }
        }
  }

  //TODO: folder not deleted
  fun deleteNoteStorage(
    noteImageRootPath: String,
    note: Note
  ) {
    note.imageList.forEach {
      storageRef.child("$noteImageRootPath/${it.fileName}")
          .delete()
    }
  }

}