package com.example.firebasedemo.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Note(
  var noteId: String = "",
  var title: String? = "",
  var message: String? = "",
  val imageList: ArrayList<NoteImage> = ArrayList()
) : Parcelable