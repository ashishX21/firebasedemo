package com.example.firebasedemo.data.model

data class User(
  var userId: String = "",
  var displayName: String? = "",
  var email: String? = "",
  val userPreferences: UserPreferences = UserPreferences()
)
