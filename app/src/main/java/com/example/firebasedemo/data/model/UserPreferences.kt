package com.example.firebasedemo.data.model

data class UserPreferences(
  var autoSave: Boolean = false
)