package com.example.firebasedemo.data.source

import com.example.firebasedemo.AppConstants.NOTES
import com.example.firebasedemo.AppConstants.USERS
import com.example.firebasedemo.data.model.Note
import com.example.firebasedemo.data.model.User
import com.example.firebasedemo.data.model.UserPreferences
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ListenerRegistration
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

class FirebaseDatabaseSource(private val database: FirebaseFirestore) {

  private fun noteRoot(userId: String) =
    database.collection(USERS)
        .document(userId)
        .collection(NOTES)

  private fun noteRootPath(userId: String) = noteRoot(userId).path

  fun noteImageRootPath(
    userId: String,
    note: Note
  ) = noteRootPath(userId) + "/${note.noteId}/images"

  fun addOrUpdateUser(user: User) = Single.create<User> { emitter ->
    database.collection(USERS)
        .document(user.userId)
        .set(user)
        .addOnCompleteListener { task ->
          if (task.isSuccessful.not()) {
            task.exception?.let {
              emitter.onError(it)
            }
          } else {
            emitter.onSuccess(user)
          }
        }
  }

  fun getCurrentUserDetails(userId: String) = Single.create<User> { emitter ->
    database.collection(USERS)
        .document(userId)
        .get()
        .addOnCompleteListener { task ->
          if (task.isSuccessful.not()) {
            task.exception?.let {
              emitter.onError(it)
            }
          } else {
            val user = task.result?.toObject(User::class.java)
            user?.let {
              emitter.onSuccess(it)
            }
          }
        }
  }

  fun setDisplayName(
    userId: String,
    displayName: String
  ) = Completable.create { emitter ->
    database.collection(USERS)
        .document(userId)
        .update(mapOf("displayName" to displayName))
        .addOnCompleteListener { task ->
          if (emitter.isDisposed.not()) {
            if (task.isSuccessful.not()) {
              task.exception?.let {
                emitter.onError(it)
              }
            } else {
              emitter.onComplete()
            }
          }
        }
  }

  fun getNotes(userId: String) = Observable.create<List<Note>> { emitter ->
    var snapshotListener: ListenerRegistration? = null
    snapshotListener = noteRoot(userId)
        .addSnapshotListener { querySnapshot, _ ->
          if (emitter.isDisposed)
            snapshotListener?.remove()
          querySnapshot?.let { snapshot ->
            emitter.onNext(snapshot.documents.mapNotNull { it.toObject(Note::class.java) })
          }
        }
  }

  fun addOrUpdateNote(
    userId: String,
    note: Note
  ) = Completable.create { emitter ->
    noteRoot(userId).document(note.noteId)
        .set(note)
        .addOnCompleteListener { task ->
          if (task.isSuccessful.not())
            task.exception?.let {
              emitter.onError(it)
            }
          emitter.onComplete()
        }
  }

  fun deleteNote(
    userId: String,
    note: Note
  ) = Completable.create { emitter ->
    noteRoot(userId).document(note.noteId)
        .delete()
        .addOnCompleteListener { task ->
          if (task.isSuccessful.not())
            task.exception?.let {
              emitter.onError(it)
            }
          emitter.onComplete()
        }
  }

  fun updateUserPreferences(
    userId: String,
    userPreferences: UserPreferences
  ) = Completable.create { emitter ->
    database.collection(USERS)
        .document(userId)
        .update(mapOf("userPreferences" to userPreferences))
        .addOnCompleteListener { task ->
          if (emitter.isDisposed.not()) {
            if (task.isSuccessful.not()) {
              task.exception?.let {
                emitter.onError(it)
              }
            } else {
              emitter.onComplete()
            }
          }
        }
  }
}