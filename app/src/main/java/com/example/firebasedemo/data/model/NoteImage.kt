package com.example.firebasedemo.data.model

import android.net.Uri
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NoteImage(
  var fileName: String? = null,
  var uriString: String? = null,
  var isUploaded: Boolean = false
) : Parcelable {
  companion object {
    fun newNoteImage(uri: Uri) = NoteImage(
        System.currentTimeMillis()
            .toString(), uri.toString(), false
    )
  }
}