package com.example.firebasedemo.data.source

import com.example.firebasedemo.data.model.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import io.reactivex.Completable
import io.reactivex.Single

class FirebaseAuthSource(private val firebaseAuth: FirebaseAuth) {

  fun loginVerify() = Single.create<Boolean> { emitter ->
    if (firebaseAuth.currentUser == null)
      emitter.onSuccess(false)
    else
      firebaseAuth.currentUser?.reload()
          ?.addOnCompleteListener {
            if (emitter.isDisposed.not()) {
              if (it.isSuccessful)
                emitter.onSuccess(firebaseAuth.currentUser != null)
              else
                emitter.onError(it.exception!!)
            }
          }
  }

  fun login(
    email: String,
    password: String
  ) = Single.create<FirebaseUser> { emitter ->
    firebaseAuth.signInWithEmailAndPassword(email, password)
        .addOnCompleteListener {task->
          if (emitter.isDisposed.not()) {
            if (task.isSuccessful)
              firebaseAuth.currentUser?.let {
                emitter.onSuccess(it)
              } ?: emitter.onError(Exception())
            else
              emitter.onError(task.exception!!)
          }
        }
  }

  fun register(
    email: String,
    password: String
  ) = Single.create<User> { emitter ->
    firebaseAuth.createUserWithEmailAndPassword(email, password)
        .addOnCompleteListener { task ->
          if (task.isSuccessful.not()) {
            task.exception?.let {
              emitter.onError(it)
            }
          } else {
            firebaseAuth.currentUser?.let {
              emitter.onSuccess(User(it.uid, "", email))
            }
          }
        }
  }

  fun sendVerificationEmail() = Completable.create { emitter ->
    firebaseAuth.currentUser?.let { firebaseUser ->
      firebaseUser.sendEmailVerification()
          .addOnCompleteListener {
            if (it.isSuccessful)
              emitter.onComplete()
            else
              emitter.onError(it.exception!!)
          }
    } ?: emitter.onError(Exception())
  }

  fun sendPasswordResetEmail(email: String) = Completable.create { emitter ->
    firebaseAuth.sendPasswordResetEmail(email)
        .addOnCompleteListener {
          if (it.isSuccessful) {
            emitter.onComplete()
          } else {
            emitter.onError(it.exception!!)
          }
        }
  }

  fun logout() = firebaseAuth.signOut()

  fun currentUser() = firebaseAuth.currentUser

}