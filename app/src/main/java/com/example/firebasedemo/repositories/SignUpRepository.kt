package com.example.firebasedemo.repositories

import com.example.firebasedemo.data.source.FirebaseAuthSource
import com.example.firebasedemo.data.source.FirebaseDatabaseSource
import com.example.firebasedemo.utils.PrefsUtil
import javax.inject.Inject

class SignUpRepository @Inject constructor(
  private val auth: FirebaseAuthSource,
  private val database: FirebaseDatabaseSource,
  private val prefsUtil: PrefsUtil
) {
  fun addNewUser(
    name: String,
    email: String,
    password: String
  ) = auth.register(email, password)
      .flatMap { user ->
        user.displayName = name
        database.addOrUpdateUser(user)
      }
      .doOnSuccess {
        prefsUtil.userId = it.userId
        prefsUtil.displayName = it.displayName
        prefsUtil.userPreferences = it.userPreferences
      }

}