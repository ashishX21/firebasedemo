package com.example.firebasedemo.repositories

import com.example.firebasedemo.data.source.FirebaseAuthSource
import com.example.firebasedemo.data.source.FirebaseDatabaseSource
import com.example.firebasedemo.utils.PrefsUtil
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class AuthRepository @Inject constructor(
  private val prefsUtil: PrefsUtil,
  private val auth: FirebaseAuthSource,
  private val database: FirebaseDatabaseSource
) {
  fun loginVerify() = auth.loginVerify()

  fun loginUser(
    email: String,
    password: String
  ) = auth.login(email, password)
      .flatMap {
        database.getCurrentUserDetails(it.uid)
      }
      .doOnSuccess {
        prefsUtil.userId = it.userId
        prefsUtil.displayName = it.displayName
        prefsUtil.userPreferences = it.userPreferences
      }
      .onErrorResumeNext {
        Single.error(it)
      }

  fun getCurrentUser() = auth.currentUser()

  fun updateDisplayName(displayName: String): Completable =
    database.setDisplayName(prefsUtil.userId, displayName)
        .doOnComplete {
          prefsUtil.displayName = displayName
        }

  fun sendVerificationEmail() = auth.sendVerificationEmail()

  fun updateUserPreferences() =
    database.updateUserPreferences(prefsUtil.userId, prefsUtil.userPreferences)

  fun sendPasswordResetEmail(email: String) = auth.sendPasswordResetEmail(email)

  fun logout() {
    auth.logout()
    prefsUtil.clear()
  }

  fun getDisplayName() = prefsUtil.displayName
}