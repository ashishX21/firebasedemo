package com.example.firebasedemo.repositories

import com.example.firebasedemo.data.model.Note
import com.example.firebasedemo.data.model.NoteImage
import com.example.firebasedemo.data.source.FirebaseDatabaseSource
import com.example.firebasedemo.data.source.FirebaseStorageSource
import com.example.firebasedemo.utils.PrefsUtil
import io.reactivex.Single
import javax.inject.Inject

class NoteRepository @Inject constructor(
  private val prefsUtil: PrefsUtil,
  private val database: FirebaseDatabaseSource,
  private val storage: FirebaseStorageSource
) {
  fun getNotes() = database.getNotes(prefsUtil.userId)

  fun addOrUpdateNote(note: Note) = database.addOrUpdateNote(prefsUtil.userId, note)

  fun deleteNote(note: Note) = database.deleteNote(prefsUtil.userId, note)
      .also {
        (storage.deleteNoteStorage(
            database.noteImageRootPath(prefsUtil.userId, note), note
        ))
      }

  fun addImageToNote(
    note: Note,
    noteImage: NoteImage
  ): Single<NoteImage> {
    return storage.uploadFile(
            database.noteImageRootPath(prefsUtil.userId, note), noteImage
        )
        .flatMap { fileUrl ->
          noteImage.uriString = fileUrl
          noteImage.isUploaded = true
          note.imageList.add(noteImage)
          database.addOrUpdateNote(prefsUtil.userId, note)
              .andThen(Single.just(noteImage))
        }
  }

  fun deleteNoteImage(
    note: Note,
    noteImage: NoteImage
  ): Single<Note> {
    note.imageList.remove(noteImage)
    return database.addOrUpdateNote(prefsUtil.userId, note)
        .andThen(storage.deleteFile(database.noteImageRootPath(prefsUtil.userId, note), noteImage))
        .andThen(Single.just(note))
  }

}