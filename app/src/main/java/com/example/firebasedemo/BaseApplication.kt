package com.example.firebasedemo

import androidx.multidex.MultiDex
import com.example.firebasedemo.injection.component.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class BaseApplication : DaggerApplication() {
  override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
    return DaggerAppComponent.builder()
        .create(this)
  }

  override fun onCreate() {
    super.onCreate()
    MultiDex.install(this)
  }

}