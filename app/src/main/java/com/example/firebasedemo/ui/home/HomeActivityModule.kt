package com.example.firebasedemo.ui.home

import android.content.Context
import com.example.firebasedemo.injection.module.BaseActivityModule
import com.example.firebasedemo.injection.qualifiers.ActivityContext
import com.example.firebasedemo.injection.scope.ActivityScope
import dagger.Binds
import dagger.Module
import dagger.android.support.DaggerAppCompatActivity

@Module(includes = [BaseActivityModule::class])
abstract class HomeActivityModule {

  @Binds
  @ActivityContext
  abstract fun provideActivityContext(activity: HomeActivity): Context

  @Binds
  @ActivityScope
  abstract fun provideActivity(hovActivity: HomeActivity): DaggerAppCompatActivity

}