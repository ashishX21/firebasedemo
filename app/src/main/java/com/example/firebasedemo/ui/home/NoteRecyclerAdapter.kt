package com.example.firebasedemo.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.firebasedemo.data.model.Note
import com.example.firebasedemo.R.layout
import com.example.firebasedemo.ui.home.NoteRecyclerAdapter.NoteViewHolder
import com.example.firebasedemo.databinding.LayoutItemNoteBinding

class NoteRecyclerAdapter(private val adapterCallback: NoteAdapterCallback) :
    ListAdapter<Note, NoteViewHolder>(diffUtil) {

  companion object{
    val diffUtil = object: DiffUtil.ItemCallback<Note>(){
      override fun areItemsTheSame(
        oldItem: Note,
        newItem: Note
      ): Boolean {
        return oldItem.noteId == newItem.noteId
      }

      override fun areContentsTheSame(
        oldItem: Note,
        newItem: Note
      ): Boolean {
        return oldItem.title == newItem.title
            && oldItem.message == newItem.message
      }

    }
  }

  class NoteViewHolder(val binding: LayoutItemNoteBinding) : RecyclerView.ViewHolder(
      binding.root
  ) {
    fun bind(note: Note) {
      binding.note = note
    }
  }

  override fun onCreateViewHolder(
    parent: ViewGroup,
    viewType: Int
  ): NoteViewHolder {
    val binding = DataBindingUtil.inflate<LayoutItemNoteBinding>(
        LayoutInflater.from(parent.context),
        layout.layout_item_note,
        parent, false
    )
    return NoteViewHolder(binding)
  }

  override fun onBindViewHolder(
    holder: NoteViewHolder,
    position: Int
  ) {
    holder.bind(getItem(holder.adapterPosition))
    holder.binding.root.setOnClickListener {
      adapterCallback.onItemClick(getItem(holder.adapterPosition))
    }
  }

  interface NoteAdapterCallback {
    fun onItemClick(note: Note)
  }
}