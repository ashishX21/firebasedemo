package com.example.firebasedemo.ui.addNote

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.firebasedemo.R
import com.example.firebasedemo.data.model.NoteImage
import com.example.firebasedemo.databinding.LayoutItemNoteImageBinding
import com.example.firebasedemo.ui.addNote.NoteImageRecyclerAdapter.NoteImageViewHolder

class NoteImageRecyclerAdapter(
  private val callback: NoteImageAdapterCallback
) : ListAdapter<NoteImage, NoteImageViewHolder>(diffUtil) {

  companion object {
    val diffUtil: DiffUtil.ItemCallback<NoteImage> = object : DiffUtil.ItemCallback<NoteImage>() {
      override fun areItemsTheSame(
        oldItem: NoteImage,
        newItem: NoteImage
      ): Boolean {
        return oldItem.uriString == newItem.uriString
      }

      override fun areContentsTheSame(
        oldItem: NoteImage,
        newItem: NoteImage
      ): Boolean {
        return (oldItem.fileName == newItem.fileName)
            && (oldItem.isUploaded == newItem.isUploaded)
      }

    }
  }

  override fun onCreateViewHolder(
    parent: ViewGroup,
    viewType: Int
  ): NoteImageViewHolder {
    val binding = DataBindingUtil.inflate<LayoutItemNoteImageBinding>(
        LayoutInflater.from(parent.context),
        R.layout.layout_item_note_image,
        parent, false
    )
    return NoteImageViewHolder(binding)
  }

  override fun onBindViewHolder(
    holder: NoteImageViewHolder,
    position: Int
  ) {
    val noteImage = getItem(holder.adapterPosition)
    holder.bind(getItem(holder.adapterPosition))
    with(holder.binding) {
      btnDelete.setOnClickListener {
        callback.deleteImage(noteImage)
      }

      root.setOnClickListener {
        Toast.makeText(root.context, noteImage.isUploaded.toString(), Toast.LENGTH_SHORT)
            .show()
      }
    }
  }

  class NoteImageViewHolder(
    val binding: LayoutItemNoteImageBinding
  ) : RecyclerView.ViewHolder(binding.root) {
    fun bind(noteImage: NoteImage) {
      binding.noteImage = noteImage
    }
  }

  interface NoteImageAdapterCallback {
    fun deleteImage(noteImage: NoteImage)
  }
}