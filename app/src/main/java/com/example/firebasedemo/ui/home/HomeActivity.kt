package com.example.firebasedemo.ui.home

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.firebasedemo.R.layout
import com.example.firebasedemo.data.model.Note
import com.example.firebasedemo.databinding.ActivityHomeBinding
import com.example.firebasedemo.ui.addNote.AddNoteActivity
import com.example.firebasedemo.ui.addNote.AddNoteActivity.Companion.KEY_NOTE_OBJECT
import com.example.firebasedemo.ui.base.BaseActivity
import com.example.firebasedemo.ui.home.NoteRecyclerAdapter.NoteAdapterCallback
import com.example.firebasedemo.ui.login.LoginActivity
import com.example.firebasedemo.utils.customView.BottomSheetMenu
import com.example.firebasedemo.utils.customView.BottomSheetMenu.BottomSheetMenuCallback

class HomeActivity : BaseActivity<ActivityHomeBinding, HomeViewModel>(),
    NoteAdapterCallback,
    BottomSheetMenuCallback {

  override fun getLayoutId() = layout.activity_home

  override fun getViewModelClass() = HomeViewModel::class.java

  private lateinit var bottomSheetMenu: BottomSheetMenu

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    binding.lifecycleOwner = this

    initUI()
    initRecyclerView()
    addListeners()
    addObservers()
    viewModel.fetchNotes()
  }

  private fun initUI() {
    bottomSheetMenu = BottomSheetMenu(this)
    binding.viewModel = viewModel
  }

  private fun addObservers() {
    viewModel.getNoteList()
        .observe(this, Observer {
          (binding.recyclerView.adapter as NoteRecyclerAdapter).submitList(it)
        })
  }

  private fun initRecyclerView() {
    binding.recyclerView.layoutManager = StaggeredGridLayoutManager(2, RecyclerView.VERTICAL)
    binding.recyclerView.adapter =
      NoteRecyclerAdapter(this)
  }

  private fun addListeners() {
    binding.btnMenu.setOnClickListener {
      if (bottomSheetMenu.isAdded && bottomSheetMenu.isVisible)
        bottomSheetMenu.dismiss()
      else
        bottomSheetMenu.show(supportFragmentManager, "bottomSheetMenu")
    }

    binding.btnAddNote.setOnClickListener {
      startActivity(
          Intent(this, AddNoteActivity::class.java)
      )
    }
  }

  override fun onItemClick(note: Note) {
    val data = Intent(this, AddNoteActivity::class.java).apply {
      putExtra(KEY_NOTE_OBJECT, note)
    }
    startActivity(data)
  }

  /* BottomSheetMenuCallback methods */
  override fun currentName(): String? = viewModel.displayName

  override fun currentAutoSaveState(): Boolean = prefsUtil.userPreferences.autoSave

  override fun nameChangeRequest(newName: String) {
    viewModel.updateDisplayName(newName)
    bottomSheetMenu.dismiss()
  }

  override fun passwordChangeRequest(newPassword: String) {
    showMessage(newPassword)
  }

  override fun onAutoSavePrefChange(isEnabled: Boolean) {
    prefsUtil.userPreferences = prefsUtil.userPreferences.apply { autoSave = isEnabled }
    bottomSheetMenu.dismiss()
    viewModel.updateUserPreferences()
  }

  override fun onLogOut() {
    viewModel.logout()
    startActivity(Intent(this, LoginActivity::class.java))
    finishAffinity()
  }
}
