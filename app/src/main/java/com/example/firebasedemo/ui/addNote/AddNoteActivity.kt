package com.example.firebasedemo.ui.addNote

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.EditText
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.firebasedemo.R
import com.example.firebasedemo.R.layout
import com.example.firebasedemo.data.model.Note
import com.example.firebasedemo.data.model.NoteImage
import com.example.firebasedemo.databinding.ActivityAddNoteBinding
import com.example.firebasedemo.ui.addNote.NoteImageRecyclerAdapter.NoteImageAdapterCallback
import com.example.firebasedemo.ui.base.BaseActivity
import com.example.firebasedemo.utils.addOnTextChangeListener
import com.example.firebasedemo.utils.visible
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.text.FirebaseVisionText
import io.reactivex.Observable
import java.util.concurrent.TimeUnit.MILLISECONDS

class AddNoteActivity : BaseActivity<ActivityAddNoteBinding, AddNoteViewModel>(),
    NoteImageAdapterCallback {

  override fun getLayoutId() = layout.activity_add_note

  override fun getViewModelClass() = AddNoteViewModel::class.java

  companion object {
    const val KEY_NOTE_OBJECT = "KEY_NOTE_OBJECT"
    const val PICK_IMAGES_TO_ADD_TO_NOTE_REQUEST_CODE = 10
    const val PICK_IMAGE_FOR_OCR_REQUEST_CODE = 11
  }

  private lateinit var note: Note

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    initUI()
    initRecyclerView()
    addListeners()
    addObservers()
  }

  private fun initUI() {
    note = intent.getParcelableExtra(KEY_NOTE_OBJECT)
        ?: Note(noteId = "I${System.currentTimeMillis()}")

    viewModel.setImageList(note.imageList)

    binding.note = note
    binding.btnSave.visible(prefsUtil.userPreferences.autoSave.not())
  }

  private fun initRecyclerView() {
    binding.recyclerView.layoutManager = StaggeredGridLayoutManager(2, RecyclerView.VERTICAL)
    binding.recyclerView.isNestedScrollingEnabled = true
    binding.recyclerView.adapter = NoteImageRecyclerAdapter(this)
  }

  private fun addObservers() {
    viewModel.getImageList()
        .observe(this, Observer {
          (binding.recyclerView.adapter as NoteImageRecyclerAdapter).submitList(it.toList())
        })

    viewModel.deleteStatus()
        .observe(this, Observer {
          if (it)
            onBackPressed()
          else
            showMessage("Unable to delete")
        })
  }

  private fun addListeners() {
    with(binding) {
      if (prefsUtil.userPreferences.autoSave) {
        addDisposable(
            etTitle.createTextChangeObservable()
                .subscribe { _ ->
                  note?.let { viewModel.addOrUpdateNote(it) }
                }
        )
        addDisposable(
            etMessage.createTextChangeObservable()
                .subscribe { _ ->
                  note?.let { viewModel.addOrUpdateNote(it) }
                }
        )
      } else {
        btnSave.setOnClickListener {
          note?.let { viewModel.addOrUpdateNote(it) }
          onBackPressed()
        }
      }

      btnDelete.setOnClickListener {
        MaterialAlertDialogBuilder(
            this@AddNoteActivity,
            R.style.AlertDialogTheme
        )
            .setTitle("Delete?")
            .setMessage("Are you sure you want to delete this image?")
            .setPositiveButton(
                "DELETE"
            ) { _, _ ->
              note?.let { viewModel.deleteNote(it) }
            }
            .setNegativeButton(
                "CANCEL"
            ) { _, _ -> }
            .show()
      }

      btnBack.setOnClickListener {
        onBackPressed()
      }

      btnOcr.setOnClickListener {
        val intentGetImage = Intent()
        intentGetImage.type = "image/*"
        intentGetImage.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(intentGetImage, PICK_IMAGE_FOR_OCR_REQUEST_CODE)
      }

      btnAddImage.setOnClickListener {
        val intentGetImages = Intent()
        intentGetImages.type = "image/*"
        intentGetImages.action = Intent.ACTION_GET_CONTENT
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
          intentGetImages.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
        }
        startActivityForResult(intentGetImages, PICK_IMAGES_TO_ADD_TO_NOTE_REQUEST_CODE)
      }
    }
  }

  private fun addImagesToNote(imageUriList: ArrayList<Uri>) {
    viewModel.addImagesToNote(note, imageUriList)
  }

  override fun onActivityResult(
    requestCode: Int,
    resultCode: Int,
    data: Intent?
  ) {
    super.onActivityResult(requestCode, resultCode, data)
    if (resultCode == Activity.RESULT_OK && data != null) {
      when (requestCode) {
        PICK_IMAGES_TO_ADD_TO_NOTE_REQUEST_CODE -> {
          val imageUriList = ArrayList<Uri>()
          if (data.clipData != null) {
            val count = data.clipData!!
                .itemCount
            for (i in 0 until count) {
              data.clipData?.let {
                imageUriList.add(it.getItemAt(i).uri)
              }
            }
          } else if (data.data != null) {
            data.data?.let {
              imageUriList.add(it)
            }
          }
          if (imageUriList.isNotEmpty())
            addImagesToNote(imageUriList)
        }
        PICK_IMAGE_FOR_OCR_REQUEST_CODE -> {
          data.data?.let { ocrImage(it) }
        }
      }
    }
  }

  private fun ocrImage(imageUri: Uri) {
    val image: FirebaseVisionImage
    try {
      image = FirebaseVisionImage.fromFilePath(this, imageUri)
      val detector = FirebaseVision.getInstance()
          .onDeviceTextRecognizer

      detector.processImage(image)
          .addOnSuccessListener {
            processFirebaseVisionText(it)
          }
          .addOnFailureListener {
            showMessage(it.toString())
          }

    } catch (e: Exception) {
      e.printStackTrace()
      Log.e("", e.toString())
      throw e
    }
  }

  private fun processFirebaseVisionText(firebaseVisionText: FirebaseVisionText) {
    if (firebaseVisionText.text.isEmpty())
      return
    val newMessage = binding.etMessage.text.toString() + "\n" + firebaseVisionText.text
    binding.etMessage.setText(newMessage)
  }

  private fun EditText.createTextChangeObservable() =
    Observable.create<String> { emitter ->
          this.addOnTextChangeListener {
            emitter.onNext(it)
          }
        }
        .debounce(1000, MILLISECONDS)

  override fun deleteImage(noteImage: NoteImage) {
    viewModel.deleteNoteImage(note, noteImage)
  }
}
