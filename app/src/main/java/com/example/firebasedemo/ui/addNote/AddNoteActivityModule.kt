package com.example.firebasedemo.ui.addNote

import android.content.Context
import com.example.firebasedemo.injection.module.BaseActivityModule
import com.example.firebasedemo.injection.qualifiers.ActivityContext
import com.example.firebasedemo.injection.scope.ActivityScope
import dagger.Binds
import dagger.Module
import dagger.android.support.DaggerAppCompatActivity

@Module(includes = [BaseActivityModule::class])
abstract class AddNoteActivityModule {

  @Binds
  @ActivityContext
  abstract fun provideActivityContext(activity: AddNoteActivity): Context

  @Binds
  @ActivityScope
  abstract fun provideActivity(hovActivity: AddNoteActivity): DaggerAppCompatActivity

}