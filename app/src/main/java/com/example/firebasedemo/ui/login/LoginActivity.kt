package com.example.firebasedemo.ui.login

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import com.example.firebasedemo.R.layout
import com.example.firebasedemo.databinding.ActivityLoginBinding
import com.example.firebasedemo.ui.base.BaseActivity
import com.example.firebasedemo.ui.home.HomeActivity
import com.example.firebasedemo.ui.signUp.SignUpActivity

class LoginActivity : BaseActivity<ActivityLoginBinding, LoginViewModel>() {

  override fun getLayoutId() = layout.activity_login

  override fun getViewModelClass() = LoginViewModel::class.java

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    addListeners()
    addObservers()
  }

  private fun addObservers() {
    viewModel.getSignInStatus()
        .observe(this, Observer {
          when (it) {
            true -> goToHomeActivity()
            false -> showMessage("Invalid Credentials")
          }
        })
  }

  private fun addListeners() {
    with(binding) {
      btnSignUp.setOnClickListener {
        startActivity(Intent(this@LoginActivity, SignUpActivity::class.java))
      }
      btnSignIn.setOnClickListener {
        viewModel.login(etEmail.text.toString(), etPassword.text.toString())
      }


      btnPasswordResetEmail.setOnClickListener {
        val email = etEmail.text.toString()
        if (email.isNotEmpty()) {
          viewModel.sendPasswordResetEmail(email)
        } else {
          showMessage("Enter a valid email")
        }
      }
    }
  }

  private fun goToHomeActivity() {
    startActivity(Intent(this, HomeActivity::class.java))
    finish()
  }
}
