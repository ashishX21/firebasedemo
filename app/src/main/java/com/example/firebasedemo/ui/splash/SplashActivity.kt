package com.example.firebasedemo.ui.splash

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import com.example.firebasedemo.R
import com.example.firebasedemo.databinding.ActivitySplashBinding
import com.example.firebasedemo.ui.base.BaseActivity
import com.example.firebasedemo.ui.home.HomeActivity
import com.example.firebasedemo.ui.login.LoginActivity

class SplashActivity : BaseActivity<ActivitySplashBinding, SplashViewModel>() {

  override fun getLayoutId(): Int = R.layout.activity_splash

  override fun getViewModelClass() = SplashViewModel::class.java

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    addObservers()
    viewModel.loginVerify()
  }

  private fun addObservers() {
    viewModel.isUserLoggedIn().observe(this, Observer {
      when(it){
        true -> goToHomeActivity()
        false -> goToLoginActivity()
      }
    })
  }

  private fun goToHomeActivity() {
    startActivity(Intent(this, HomeActivity::class.java))
    finish()
  }

  private fun goToLoginActivity() {
    startActivity(Intent(this, LoginActivity::class.java))
    finish()
  }

}
