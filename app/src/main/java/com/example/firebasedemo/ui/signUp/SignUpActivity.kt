package com.example.firebasedemo.ui.signUp

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import com.example.firebasedemo.R
import com.example.firebasedemo.databinding.ActivitySignUpBinding
import com.example.firebasedemo.ui.base.BaseActivity
import com.example.firebasedemo.ui.home.HomeActivity

class SignUpActivity : BaseActivity<ActivitySignUpBinding, SignUpViewModel>() {

  override fun getLayoutId() = R.layout.activity_sign_up

  override fun getViewModelClass() = SignUpViewModel::class.java

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    addListeners()
    addObservers()
  }

  private fun addObservers(){
    viewModel.getSignUpStatus().observe(this, Observer {
      when(it){
        true -> goToHomeActivity()
        false -> showMessage("Invalid Credentials")
      }
    })
  }

  private fun addListeners(){
    with(binding) {
      btnSignUp.setOnClickListener {
        viewModel.signUp(
          etName.text.toString(),
          etEmail.text.toString(),
          etPassword.text.toString()
        )
      }
    }
  }

  private fun goToHomeActivity() {
    startActivity(Intent(this, HomeActivity::class.java))
    finishAffinity()
  }
}
