package com.example.firebasedemo.ui.splash

import androidx.lifecycle.MutableLiveData
import com.example.firebasedemo.repositories.AuthRepository
import com.example.firebasedemo.ui.base.BaseViewModel
import javax.inject.Inject

class SplashViewModel @Inject constructor(private val authRepository: AuthRepository) :
    BaseViewModel() {

  private val mLoginState = MutableLiveData<Boolean>()

  fun isUserLoggedIn() = mLoginState

  fun loginVerify() {
    addDisposable(
        authRepository.loginVerify()
            .subscribe({
              mLoginState.postValue(it)
            }, {
              mLoginState.postValue(false)
            })
    )
  }

}