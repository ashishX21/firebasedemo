package com.example.firebasedemo.ui.addNote

import android.net.Uri
import androidx.lifecycle.MutableLiveData
import com.example.firebasedemo.data.model.Note
import com.example.firebasedemo.data.model.NoteImage
import com.example.firebasedemo.repositories.NoteRepository
import com.example.firebasedemo.ui.base.BaseViewModel
import javax.inject.Inject

class AddNoteViewModel @Inject constructor(private val noteRepository: NoteRepository) :
    BaseViewModel() {

  private val mImageList = MutableLiveData<ArrayList<NoteImage>>()//TODO: fix this
  private val mDeletedStatus = MutableLiveData<Boolean>()

  fun getImageList() = mImageList

  fun deleteStatus() = mDeletedStatus

  fun addOrUpdateNote(note: Note) {
    addDisposable(
        noteRepository.addOrUpdateNote(note)
            .subscribe({
            }, {
              handleException(it)
            })
    )
  }

  fun setImageList(imageUriList: ArrayList<NoteImage>) {
    mImageList.postValue(
        imageUriList
    )
  }

  private fun addNoteImage(
    note: Note,
    imageUri: Uri
  ) {
    addDisposable(
        noteRepository.addImageToNote(
                note,
                NoteImage.newNoteImage(imageUri)
            )
            .subscribe({
              mImageList.postValue(mImageList.value)//Working, but why?.
            }, {
              handleException(it)
            })
    )
  }

  fun addImagesToNote(
    note: Note,
    imageUriList: ArrayList<Uri>
  ) {
    imageUriList.forEach {
      addNoteImage(note, it)
    }
  }

  fun deleteNoteImage(
    note: Note,
    noteImage: NoteImage
  ) {
    addDisposable(
        noteRepository.deleteNoteImage(note, noteImage)
            .subscribe({
              mImageList.postValue(it.imageList)
            }, {
              handleException(it)
            })
    )
  }

  fun deleteNote(note: Note) {
    setProgress(true)
    addDisposable(
        noteRepository.deleteNote(note)
            .subscribe({
              setProgress(false)
              mDeletedStatus.postValue(true)
            }, {
              mDeletedStatus.postValue(false)
              handleException(it)
            })
    )
  }
}

