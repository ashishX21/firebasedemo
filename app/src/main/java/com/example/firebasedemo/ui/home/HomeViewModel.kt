package com.example.firebasedemo.ui.home

import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableString
import android.text.style.StyleSpan
import androidx.lifecycle.MutableLiveData
import com.example.firebasedemo.data.model.Note
import com.example.firebasedemo.repositories.AuthRepository
import com.example.firebasedemo.repositories.NoteRepository
import com.example.firebasedemo.ui.base.BaseViewModel
import com.example.firebasedemo.utils.firstWord
import javax.inject.Inject

class HomeViewModel @Inject constructor(
  private val noteRepository: NoteRepository,
  private val authRepository: AuthRepository
) : BaseViewModel() {

  private val noteList = MutableLiveData<List<Note>>()

  var displayName = authRepository.getDisplayName()
    private set

  var displayTitle = MutableLiveData(getTitle())
    private set

  fun getNoteList() = noteList

  private fun getTitle(): Spannable {
    val textNotes = "Notes"
    val textToDisplay =
      "${displayName.firstWord()}'s $textNotes"

    val spannableContent = SpannableString(textToDisplay)
    spannableContent.setSpan(
        StyleSpan(Typeface.BOLD), textToDisplay.indexOf(textNotes), textToDisplay.length,
        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
    )
    return spannableContent
  }

  fun fetchNotes() {
    addDisposable(
        noteRepository.getNotes()
            .subscribe({
              noteList.postValue(it)
            }, {
              handleException(it)
            })
    )
  }

  fun updateDisplayName(displayName: String) {
    setProgress(true)
    addDisposable(
        authRepository.updateDisplayName(displayName)
            .subscribe({
              setProgress(false)
              this.displayName = displayName
              displayTitle.postValue(getTitle())
            }, {
              handleException(it)
            })
    )
  }

  fun logout() {
    authRepository.logout()
  }

  fun updateUserPreferences() {
    setProgress(true)
    addDisposable(
        authRepository.updateUserPreferences()
            .subscribe({
              setProgress(false)
            }, {
              handleException(it)
            })
    )
  }

}