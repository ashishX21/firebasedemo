package com.example.firebasedemo.ui.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.firebasedemo.utils.SingleLiveEvent
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseViewModel : ViewModel() {
  private val mProgress = MutableLiveData<Boolean>()
  private val mMessage = SingleLiveEvent<String>()
  fun getProgress(): LiveData<Boolean> = mProgress
  fun showMessage() = mMessage
  private var compositeDisposable: CompositeDisposable? = null

  override fun onCleared() {
    super.onCleared()
    compositeDisposable?.let {
      it.dispose()
      it.clear()
      compositeDisposable = null
    }
  }

  protected fun addDisposable(disposable: Disposable) {
    if (compositeDisposable == null) {
      compositeDisposable = CompositeDisposable()
    }
    compositeDisposable!!.add(disposable)
  }

  protected fun setProgress(value: Boolean) {
    mProgress.postValue(value)
  }

  protected fun showMessage(message: String) {
    mMessage.postValue(message)
  }

  protected fun handleException(e: Throwable) {
    setProgress(false)
    throw e
  }
}