package com.example.firebasedemo.ui.signUp

import androidx.lifecycle.MutableLiveData
import com.example.firebasedemo.repositories.SignUpRepository
import com.example.firebasedemo.ui.base.BaseViewModel
import com.example.firebasedemo.utils.PrefsUtil
import javax.inject.Inject

class SignUpViewModel @Inject constructor(
  private val signUpRepository: SignUpRepository
) :
    BaseViewModel() {

  private val mSignUpComplete = MutableLiveData<Boolean>()

  fun getSignUpStatus() = mSignUpComplete

  fun signUp(
    name: String,
    email: String,
    password: String
  ) {
    setProgress(true)
    addDisposable(
        signUpRepository.addNewUser(name, email, password)
            .subscribe({
              setProgress(false)
              mSignUpComplete.postValue(true)
            }, {
              handleException(it)
            })
    )
  }
}