package com.example.firebasedemo.ui.login

import androidx.lifecycle.MutableLiveData
import com.example.firebasedemo.repositories.AuthRepository
import com.example.firebasedemo.ui.base.BaseViewModel
import javax.inject.Inject

class LoginViewModel @Inject constructor(
  private val authRepository: AuthRepository
) : BaseViewModel() {

  private val mSignInComplete = MutableLiveData<Boolean>()

  fun getSignInStatus() = mSignInComplete

  fun login(
    email: String,
    password: String
  ) {
    setProgress(true)
    addDisposable(
        authRepository.loginUser(email, password)
            .subscribe({
              setProgress(false)
              mSignInComplete.postValue(true)
            }, {
              handleException(it)
              mSignInComplete.postValue(false)
            })
    )
  }

  fun sendPasswordResetEmail(email: String) {
    addDisposable(
        authRepository.sendPasswordResetEmail(email)
            .subscribe({
              showMessage("Password reset email sent")
            }, {
              handleException(it)
            })
    )
  }
}