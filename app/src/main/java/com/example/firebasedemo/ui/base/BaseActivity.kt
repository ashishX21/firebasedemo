package com.example.firebasedemo.ui.base

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.firebasedemo.utils.PrefsUtil
import com.example.firebasedemo.utils.UiUtils
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import javax.inject.Inject

abstract class BaseActivity<B : ViewDataBinding, VM : BaseViewModel> : DaggerAppCompatActivity() {

  protected lateinit var binding: B
  protected lateinit var viewModel: VM
  private var compositeDisposable: CompositeDisposable? = null

  @Inject
  lateinit var viewModelFactory: ViewModelProvider.Factory

  @Inject
  lateinit var uiUtils: UiUtils

  @Inject
  lateinit var prefsUtil: PrefsUtil

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    binding = DataBindingUtil.setContentView(this, getLayoutId())
    viewModel = ViewModelProvider(this, viewModelFactory)[getViewModelClass()]
    uiUtils = UiUtils(this)

    viewModel.getProgress()
        .observe(this, Observer {
          if (it) showProgress() else hideProgress()
        })

    viewModel.showMessage()
        .observe(this, Observer {
          showMessage(it)
        })
  }

  protected fun addDisposable(disposable: Disposable) {
    if (compositeDisposable == null) {
      compositeDisposable = CompositeDisposable()
    }
    compositeDisposable!!.add(disposable)
  }

  protected fun showProgress() {
    uiUtils.showProgress(supportFragmentManager)
  }

  protected fun hideProgress() {
    uiUtils.hideProgress()
  }

  @LayoutRes abstract fun getLayoutId(): Int

  abstract fun getViewModelClass(): Class<VM>

  protected fun showMessage(message: String) {
    uiUtils.showMessage(message)
  }

  override fun onDestroy() {
    super.onDestroy()
    compositeDisposable?.let {
      it.dispose()
      it.clear()
      compositeDisposable = null
    }
  }
}