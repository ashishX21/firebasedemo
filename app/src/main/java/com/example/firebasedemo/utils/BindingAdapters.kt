package com.example.firebasedemo.utils

import android.net.Uri
import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso

@BindingAdapter("imageFull")
fun imageFull(
  view: ImageView,
  uriString: String
) {
  Picasso.get()
      .load(Uri.parse(uriString))
      .into(view)
}

@BindingAdapter("isVisible")
fun setVisibility(
  view: View,
  isVisible: Boolean
) {
  view.visible(isVisible)
}