package com.example.firebasedemo.utils

import android.annotation.SuppressLint
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText

fun View.visible(isVisible: Boolean) {
  this.visibility = if (isVisible) View.VISIBLE else View.GONE
}

@SuppressLint("ClickableViewAccessibility")
fun EditText.setOnDrawableEndClickListener(func: () -> Unit) {
  this.setOnTouchListener { _, event ->
    val drawableRight = 2
    if (event.action == MotionEvent.ACTION_UP) {
      if (event.rawX >= (this.right - this.compoundDrawables[drawableRight].bounds.width())) {
        func.invoke()
        return@setOnTouchListener true
      }
    }
    return@setOnTouchListener false
  }
}

fun EditText.addOnTextChangeListener(func: (String) -> Unit) {
  this.addTextChangedListener(
      object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {}

        override fun beforeTextChanged(
          s: CharSequence?,
          start: Int,
          count: Int,
          after: Int
        ) {
        }

        override fun onTextChanged(
          s: CharSequence?,
          start: Int,
          before: Int,
          count: Int
        ) {
          func.invoke(s.toString())
        }

      }
  )
}

fun EditText.setOnActionDoneListener(func: (String) -> Unit) {
  this.setOnEditorActionListener { v, actionId, event ->
    if (actionId == EditorInfo.IME_ACTION_DONE
        || event.action == KeyEvent.ACTION_DOWN
        && event.keyCode == KeyEvent.KEYCODE_ENTER
    ) {
      func.invoke(v.text.toString())
      return@setOnEditorActionListener true
    }
    return@setOnEditorActionListener true
  }
}

fun String?.firstWord(): String?{
  return if (this == null) null
  else{
    val string = this.trim()
    if (string.contains(" "))
      string.substring(0, string.indexOf(" "))
    else string
  }
}