package com.example.firebasedemo.utils.customView

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.example.firebasedemo.R
import com.example.firebasedemo.databinding.LayoutCustomProgressDialogBinding

class CustomProgressDialog: DialogFragment(){

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    val binding = DataBindingUtil.inflate<LayoutCustomProgressDialogBinding>(
        inflater, R.layout.layout_custom_progress_dialog, container, false
    )
    return binding.root
  }

}