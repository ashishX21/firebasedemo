package com.example.firebasedemo.utils

import android.content.SharedPreferences
import com.example.firebasedemo.data.model.UserPreferences
import javax.inject.Inject

class PrefsUtil @Inject constructor(private val pref: SharedPreferences) {

  companion object {
    const val KEY_AUTO_SAVE = "KEY_AUTO_SAVE"
    const val KEY_USER_ID = "KEY_USER_ID"
    const val KEY_DISPLAY_NAME = "KEY_DISPLAY_NAME"
  }

  private var autoSave: Boolean
    get() = pref.getBoolean(KEY_AUTO_SAVE, false)
    set(value) = pref.edit()
        .putBoolean(KEY_AUTO_SAVE, value)
        .apply()

  var userId: String
    get() = pref.getString(KEY_USER_ID, "") ?: ""
    set(value) = pref.edit()
        .putString(KEY_USER_ID, value)
        .apply()

  var displayName: String?
    get() = pref.getString(KEY_DISPLAY_NAME, null)
    set(value) = pref.edit()
        .putString(KEY_DISPLAY_NAME, value)
        .apply()

  var userPreferences: UserPreferences
    get() = UserPreferences(autoSave)
    set(value) {
      autoSave = value.autoSave
    }

  fun clear() {
    pref.edit()
        .clear()
        .apply()
  }

}