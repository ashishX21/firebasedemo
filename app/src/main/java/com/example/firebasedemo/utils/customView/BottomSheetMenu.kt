package com.example.firebasedemo.utils.customView

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.databinding.DataBindingUtil
import com.example.firebasedemo.R
import com.example.firebasedemo.databinding.LayoutBottomSheetMenuBinding
import com.example.firebasedemo.utils.setOnActionDoneListener
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class BottomSheetMenu(
  private val callback: BottomSheetMenuCallback
) :
    BottomSheetDialogFragment() {

  private lateinit var binding: LayoutBottomSheetMenuBinding

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    binding = DataBindingUtil.inflate(
        inflater, R.layout.layout_bottom_sheet_menu, container, false
    )
    return binding.root
  }

  @SuppressLint("ClickableViewAccessibility")
  override fun onViewCreated(
    view: View,
    savedInstanceState: Bundle?
  ) {
    super.onViewCreated(view, savedInstanceState)
    with(binding) {
      etName.setText(callback.currentName())
      switchAutoSave.isChecked = callback.currentAutoSaveState()

      switchAutoSave.setOnCheckedChangeListener { _, isChecked ->
        callback.onAutoSavePrefChange(isChecked)
      }

      btnEditName.setOnClickListener {
        etName.isEnabled = true
        etName.requestFocus(EditText.FOCUS_LEFT)
      }

      btnEditPassword.setOnClickListener {
        etPassword.setText("")
        etPassword.isEnabled = true
        etPassword.requestFocus()
      }

      etName.setOnActionDoneListener {
        callback.nameChangeRequest(it)
        etName.clearFocus()
      }

      etPassword.setOnActionDoneListener {
        callback.passwordChangeRequest(it)
        etName.clearFocus()
      }

      etName.setOnFocusChangeListener { v, hasFocus ->
        v.isEnabled = hasFocus
      }

      etPassword.setOnFocusChangeListener { v, hasFocus ->
        v.isEnabled = hasFocus
      }

      btnLogout.setOnClickListener { callback.onLogOut() }

    }

  }

  interface BottomSheetMenuCallback {
    fun currentName(): String?
    fun currentAutoSaveState(): Boolean
    fun nameChangeRequest(newName: String)
    fun passwordChangeRequest(newPassword: String)
    fun onAutoSavePrefChange(isEnabled: Boolean)
    fun onLogOut()
  }

}